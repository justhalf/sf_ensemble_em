# Ensembling SF type predictions

Usage:

```shell
usage: ensemble_em.py [-h] --sys_out_files SYS_OUT_FILES [SYS_OUT_FILES ...] [--out_path OUT_PATH] {union,intersection,general_truthfinder} ...

To do ensemble of multiple systems

positional arguments:
  {union,intersection,general_truthfinder}
                        The ensemble method
    union               Use union ensemble method.
    intersection        Use intersection ensemble method.
    general_truthfinder
                        Use General TruthFinder ensemble method.

optional arguments:
  -h, --help            show this help message and exit
  --sys_out_files SYS_OUT_FILES [SYS_OUT_FILES ...]
                        The list of system output files to be used as the basis for ensemble.
  --out_path OUT_PATH   The output path. If not provided, will be printed to standard output.
```

Sample:
```shell
python code/ensemble_em.py --sys_out_files /usr1/data/ruochenx/real_run_aug_2017/submit/il5/bwe_xlingual_Graham_KWD/dom_mmatch_tgt_train_0/out.json /usr1/data/ruochenx/real_run_aug_2017/submit/il5/bwe_xlingual_AB/dom_mmatch_tgt_train_1/out.json /usr1/data/ruochenx/real_run_aug_2017/submit/il5/bwe_xlingual_ABD/dom_mmatch_tgt_train_1/out.json /usr1/data/ruochenx/real_run_aug_2017/submit/il5/bwe_xlingual_ABCD/dom_mmatch_tgt_train_1/out.json --out_path ~/data/ensemble2018/preds/il5/nn/generalized-tf/A+AB+ABD+ABCD.multi.json general_truthfinder --reliability multi
```
