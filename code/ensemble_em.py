# -*- coding: utf-8 -*-
"""
01 Jun 2018
To do ensemble of multiple systems
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import json
from lor_utils import SituationFrame as SF, Version
import numpy as np
from collections import OrderedDict
from copy import copy
from tqdm import tqdm

SF_TYPES = ['terrorism', 'crimeviolence', 'regimechange', 'food', 'water', 'med', 'infra', 'shelter', 'evac', 'utils', 'search']
SF_TYPES_VOCAB = dict([(sf_type, idx) for (idx, sf_type) in enumerate(SF_TYPES)])

def read_sys_out(sys_out_file):
    '''Read system output'''
    with open(sys_out_file, 'r') as infile:
        return [SF.from_dict(sf) for sf in json.load(infile)]

def list_to_mat(sys_out, doc_id_seg_ids):
    '''Convert a list of system output into a matrix.'''
    result = np.zeros((len(doc_id_seg_ids), len(SF_TYPES)))
    for sf in sys_out:
        key = (sf.doc_id, sf.seg_id)
        row_idx = doc_id_seg_ids[key][0]
        sf_type = sf.type
        sf_type_idx = SF_TYPES_VOCAB[sf_type]
        confidence = sf.type_confidence
        if result[row_idx, sf_type_idx] != 0:
            print('WARNING: the same SF type ({}) predicted twice for the same segment: {}'.format(sf_type, key), file=sys.stderr)
        result[row_idx, sf_type_idx] = confidence
    return result

def lists_to_mats(sys_outs):
    '''Convert lists of system output into a matrix.'''
    doc_id_seg_ids = OrderedDict()
    for sys_out in sys_outs:
        for sf in sys_out:
            key = (sf.doc_id, sf.seg_id)
            if key not in doc_id_seg_ids:
                doc_id_seg_ids[key] = (len(doc_id_seg_ids), {})
            doc_id_seg_ids[key][1][sf.type] = sf
    return ([list_to_mat(sys_out, doc_id_seg_ids) for sys_out in sys_outs], doc_id_seg_ids)

def mat_to_list(ensemble_out_mat, doc_id_seg_ids, sys_out_mats, method, auxiliary):
    doc_id_seg_id_list = list(doc_id_seg_ids.values())
    result = []
    for row_idx, row in enumerate(ensemble_out_mat):
        orig_sf = doc_id_seg_id_list[row_idx][1]
        for sf_type_idx, confidence in enumerate(row):
            if confidence == 0:
                continue
            sf_type = SF_TYPES[sf_type_idx]
            new_sf = copy(orig_sf[sf_type])
            new_sf.type = sf_type
            new_sf.type_confidence = confidence
            new_sf.source = 'ensemble-{}'.format(method)
            new_sf_dict = new_sf.to_dict(version=Version.v2018)
            new_sf_dict['type_confidence_list'] = [sys_out_mat[row_idx, sf_type_idx] for sys_out_mat in sys_out_mats]
            new_sf_dict['auxiliary'] = auxiliary
            result.append(new_sf_dict)
    return result

def do_ensemble(sys_out_mats, method, args={}):
    auxiliary = None
    if method == 'union':
        ensemble_out_mat = 1-sys_out_mats[0]
        for sys_out_mat in sys_out_mats[1:]:
            ensemble_out_mat = np.multiply(ensemble_out_mat, 1-sys_out_mat)
        ensemble_out_mat = 1-ensemble_out_mat
    elif method == 'intersect':
        ensemble_out_mat = sys_out_mats[0]
        for sys_out_mat in sys_out_mats[1:]:
            ensemble_out_mat = np.multiply(ensemble_out_mat, sys_out_mat)
    elif method == 'general_truthfinder':
        num_iter = args.numiter
        dim_sys = 1 if args.reliability == 'uni' else 11
        sys_reliabilities = np.ones((len(sys_out_mats), dim_sys)) * 0.5
        next_sys_reliabilities = np.array(sys_reliabilities, copy=True)
        iternum = 0
        while True:
            ensemble_out_mat = np.ones(sys_out_mats[0].shape)
            for sys_idx, sys_reliability in enumerate(sys_reliabilities):
                ensemble_out_mat = np.multiply(ensemble_out_mat, (1-sys_reliability)**sys_out_mats[sys_idx])
            ensemble_out_mat = 1-ensemble_out_mat
            for sys_idx, sys_reliability in enumerate(sys_reliabilities):
                axis = None if dim_sys == 1 else 0
                next_sys_reliabilities[sys_idx] = np.sum(np.multiply(sys_out_mats[sys_idx], ensemble_out_mat), axis=axis) / np.sum(sys_out_mats[sys_idx], axis=axis)
            loss = np.linalg.norm(next_sys_reliabilities-sys_reliabilities)
            print('Loss at iter {}: {:.8f}'.format(iternum, loss))
            sys_reliabilities = np.array(next_sys_reliabilities, copy=True)
            iternum += 1
            if iternum >= num_iter:
                break
        auxiliary = {}
        auxiliary['sys_reliabilities'] = sys_reliabilities.tolist()
    return ensemble_out_mat, auxiliary

def main():
    parser = ArgumentParser(description='To do ensemble of multiple systems')
    parser.add_argument('--sys_out_files', nargs='+', required=True,
                        help='The list of system output files to be used as the basis for ensemble.')
    parser.add_argument('--out_path',
                        help='The output path. If not provided, will be printed to standard output.')
    subparsers = parser.add_subparsers(dest='method', help='The ensemble method')
    union_method = subparsers.add_parser('union',
                                         help='Use union ensemble method.')
    intersect_method = subparsers.add_parser('intersection',
                                             help='Use intersection ensemble method.')
    general_truthfinder_method = subparsers.add_parser('general_truthfinder',
                                                       help='Use General TruthFinder ensemble method.')
    general_truthfinder_method.add_argument('--numiter', type=int, default=20,
                                            help='The number of iterations')
    general_truthfinder_method.add_argument('--reliability', choices=['uni', 'multi'],
                                            default='uni', help='dimension of a reliability variable')
    args = parser.parse_args()

    # Read the arguments
    sys_out_files = args.sys_out_files
    out_path = args.out_path
    method = args.method

    # Read system outputs
    sys_outs = [read_sys_out(sys_out_file) for sys_out_file in sys_out_files]
    sys_out_mats, doc_id_seg_ids = lists_to_mats(sys_outs)

    # Do ensemble
    ensemble_out_mat, auxiliary = do_ensemble(sys_out_mats, method=method, args=args)
    ensemble_out = mat_to_list(ensemble_out_mat, doc_id_seg_ids, sys_out_mats, method, auxiliary)

    # Print outputs
    if out_path is None:
        json.dump(ensemble_out, sys.stdout, sort_keys=True, indent=4, ensure_ascii=False)
    else:
        with open(out_path, 'w') as outfile:
            json.dump(ensemble_out, outfile, sort_keys=True, indent=4, ensure_ascii=False)

if __name__ == '__main__':
    main()

