# -*- coding: utf-8 -*-
"""
29 May 2018
Test the idea of EM
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import numpy as np

def main():
    parser = ArgumentParser(description='')
    parser.add_argument('--max_iter', type=int, default=20,
                        help='The maximum number of iterations.')
    parser.add_argument('--combination', choices=['average', 'disjunctive'], default='average',
                        help='The method to combine confidence scores from various systems.')
    args = parser.parse_args()
    m1 = np.array([[1,1,0],[1,0,1],[0,0,1],[1,0,0]], np.float)
    m2 = np.array([[1,1,0],[0,1,1],[0,0,1],[1,1,0]], np.float)
    m1 = np.array([[0.9,0.5,0.01],[0.8,0   ,0.6],[0.01,0.03,0.5],[0.9,0.01,0.02]], np.float)
    m2 = np.array([[0  ,1  ,0   ],[0  ,1   ,0  ],[0   ,0   ,0  ],[0  ,1   ,0   ]], np.float)
    cur_m1 = 1
    cur_m2 = 1
    numel = m1.size
    max_iter = args.max_iter
    iternum = 0
    print(m1)
    print(m2)
    while True:
        if iternum == max_iter-1:
            print('Iteration {}'.format(iternum))
            print('m1: {:.8f}'.format(cur_m1))
            print('m2: {:.8f}'.format(cur_m2))
        if args.combination == 'disjunctive':
            combined = 1-(np.multiply((1-cur_m1*m1)**m1, (1-cur_m2*m2)**m2))
        else:
            combined = (cur_m1*m1+cur_m2*m2)/(cur_m1+cur_m2)
        if iternum == max_iter-1:
            print('SF Probabilities:')
            print(combined)
        # next_m1 = np.sum(np.multiply(combined, m1))/(numel)
        # next_m2 = np.sum(np.multiply(combined, m2))/(numel)
        next_m1 = np.sum(np.multiply(combined, m1))/(np.sum(m1))
        next_m2 = np.sum(np.multiply(combined, m2))/(np.sum(m2))
        # next_sum = next_m1+next_m2
        # next_sum = max(next_m1, next_m2)
        # next_m1 /= next_sum
        # next_m2 /= next_sum
        loss = np.sqrt((next_m1-cur_m1)**2 + (next_m2-cur_m2)**2)
        if iternum == max_iter-1:
            print('Loss: {:.8f}'.format(loss))
        iternum += 1
        cur_m1 = next_m1
        cur_m2 = next_m2
        if iternum >= max_iter:
            break

if __name__ == '__main__':
    main()

